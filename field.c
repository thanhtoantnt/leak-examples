#include <stdio.h>
#include <stdlib.h>

struct IStruct {
  int*   id;
};

struct IStruct* test_fun(int val) {
  struct IStruct* table;
  table = malloc(sizeof(struct IStruct)); //o1
  if (table == NULL)
    return NULL;

  table-> id = malloc(1);
  if (!table->id) {
    free(table);
    return NULL;
  }
  if (val > 3){
    return NULL;
  }


  return table;
}


int main() {
  struct IStruct* table;
  table = test_fun(5);

  return 1;
}
