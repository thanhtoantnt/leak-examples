#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct listNode {
  struct listNode *next;
  int val;
} listNode;

bool listAddNode(listNode *alist, int val)
{
  if (alist == NULL) return false;
  listNode *new_node;
  if ((new_node = malloc(sizeof(*new_node))) == NULL){
    return false;
  }
  struct listNode *tmp;
  tmp = alist;
  while (tmp -> next != NULL) tmp = tmp -> next;

  new_node->next = NULL;
  new_node->val = val;
  tmp -> next = new_node;
  return true;
}

/* summary: '_Bool listAddNode(listNode* alist, int val)
PRE:
PURE:
alist != null ;@PURE_END
SIGMA:val = val$2<formal>:int; alist = val$1<formal>:listNode* ;
lseg_ne(alist,null,[],P0)SIGMA_ENDwhere
P0 = _t$0|->{next:_t$1<rearrange:20>}<rearrange:20>:listNode

POST 1 of 3:
PURE:
alist != null ;@PURE_END
SIGMA:val = val$2<formal>:int; return = 0<update:16>:_Bool; alist = val$1<formal>:listNode* ;
lseg_ne(alist,null,[],P0)SIGMA_ENDwhere
P0 = _t$0|->{next:_t$1<formal>}<formal>:listNode

POST 2 of 3:
PURE:
val$3 != null; alist != null; MEMma<malloc:15>(val$3) ;@PURE_END
SIGMA:val = val$2<formal>:int; return = 1<update:25>:_Bool; alist = val$1<formal>:listNode* ;
val$3|->{next:null<update:22>, val:val<update:23>}<alloc>:listNode16;
lseg_ne(alist,val$3,[],P0)SIGMA_END
 where
P0 = _t$0|->{next:_t$1<formal>}<formal>:listNode

POST 3 of 3:
PURE:
val$3 != null; alist != null; MEMma<malloc:15>(val$3) ;@PURE_END
SIGMA:val = val$2<formal>:int; return = 1<update:25>:_Bool; alist = val$1<formal>:listNode* ;
val$3|->{next:null<update:22>, val:val<update:23>}<alloc>:listNode16;
alist|->{next:val$3<update:24>}<formal>:listNodeSIGMA_END

*/

void printlist(listNode *plist){
  if (plist != NULL){
    printf("printxx val is %d \n", plist->val);
    printlist(plist->next);
  }

}

int main() {
  listNode* mlist;
  if ((mlist = malloc(sizeof(*mlist))) == NULL)
    return -1;
  mlist->next = NULL;
  mlist->val = 1;

  bool b1;
  b1 = listAddNode(mlist, 3);
  if (!b1)
    return -1; // leak #1
  /* state at leak #1:
     memory hpred: 'lseg_ne(_t$172,null,[],lam [_t$0,_t$1,]. exists [].
     _t$0|->{next:_t$1<formal>}<formal>:listNode)'

     memory hpred: 'lseg_ne(_t$172,null,[],lam [_t$0,_t$1,]. exists []. _t$0|->{next:_t$1<formal>}<formal>:listNode)'
     prop: 'PURE:
     _t$172 != null; MEMma<malloc:38>(_t$172) ;@PURE_END

     SIGMA:b3 = _t$169<initial>:_Bool; b2 = _t$170<initial>:_Bool; mlist = 0<nullify>:listNode*; return = -1<update:46>:int; b1 = 0<nullify>:_Bool ;
     lseg_ne(_t$172,null,[],P0)SIGMA_END where
     P0 = _t$0|->{next:_t$1<formal>}<formal>:listNode'

*/

  bool b2;
  b2 = listAddNode(mlist, 5);
  if (!b2)
    return -1; // leak #2
  /* state at leak #2
     memory hpred: 'lseg_ne(_t$291,null,[],lam [_t$0,_t$1,]. exists [].
     _t$0|->{next:_t$1<formal>}<formal>:listNode)'

     memory_leak prop: 'PURE:
     _t$292 != null; _t$291 != null; MEMma<listAddNode:44>(_t$292);
     MEMma<malloc:38>(_t$291) ;@PURE_END

     SIGMA:b3 = _t$289<initial>:_Bool; b2 = 0<nullify>:_Bool; mlist = 0<nullify>:listNode*; return = -1<update:50>:int; b1 = 0<nullify>:_Bool ;
     lseg_ne(_t$291,null,[],P0)SIGMA_END where
     P0 = _t$0|->{next:_t$1<formal>}<formal>:listNode'
*/

  bool b3;
  b3 = listAddNode(mlist, 7);
  if (!b3)
    return -1; // leak #3

  printlist(mlist);

  return 0;   // leak #4
}

