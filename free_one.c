#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct listNode {
  struct listNode *next;
  int val;
} listNode;

bool listAddNode(listNode *alist, int val)
{
  if (alist == NULL) return false;
  listNode *new_node;
  if ((new_node = malloc(sizeof(*new_node))) == NULL){
    return false;
  }
  struct listNode *tmp;
  tmp = alist;
  while (tmp -> next != NULL) tmp = tmp -> next;

  new_node->next = NULL;
  new_node->val = val;
  tmp -> next = new_node;
  return true;
}

void printlist(listNode *plist){
  if (plist != NULL){
    printf("printxx val is %d \n", plist->val);
    printlist(plist->next);
  }

}

int main() {
  listNode* mlist;
  if ((mlist = malloc(sizeof(*mlist))) == NULL)
    return -1;
  mlist->next = NULL;
  mlist->val = 1;

  bool b1;
  b1 = listAddNode(mlist, 3);
  if (!b1)
    return -1;
  bool b2;
  b2 = listAddNode(mlist, 5);
  if (!b2){
    free(mlist->next);
    return -1;
  }
  bool b3;
  b3 = listAddNode(mlist, 7);
  if (!b3)
    return -1;

  printlist(mlist);

  return 0;
}

