#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct listNode {
  struct listNode *next;
  int val;
} listNode;

listNode *nodeCreate(int val)
{
  struct listNode *ilist;

  if ((ilist = malloc(sizeof(*ilist))) == NULL)
    return NULL;
  ilist -> next = NULL;
  ilist -> val = val;

  for (int i = 0; i <= 10; i++){
    /* need loop invariant */
    struct listNode *newNode;
    if ((newNode = malloc(sizeof(*newNode))) == NULL){
      return NULL; // leak 1
      /* hpred: '_t$131|->{next:null<update:16>,
         val:@f$0<update:17>}<alloc>:listNode16' */

      /* simplified */
      /* hpred: ilist |->{next: null, val:_} */

      /* \/ OR */
      /* PURE: MEMma<malloc:14>(mlist); MEMma<malloc:22>(mnext); MEMma<malloc:22>(mnextnext)

         HEAP: mlist|->{next:mnext<update:36>, val:old_val<update:37>}<alloc>:listNode16;
         mnext|->{next:mnextnext<update:36>, val:(@f$0 + 1)<update:37>}<alloc>:listNode16;
         mnextnext|->{next:@f$1<alloc>, val:@f$2<alloc>}<alloc>:listNode16
      */

    }
    struct listNode *tmp;
    tmp = ilist;
    while (tmp -> next != NULL){
      tmp = tmp -> next;
    }
    newNode -> val = val + i;
    tmp -> next = newNode;
  }
  return ilist;
}

/* void printlist(listNode *plist){ */
/*   if (plist != NULL){ */
/*     printf("printxx val is %d \n", plist->val); */
/*     printlist(plist->next); */
/*   } */

/* } */


/* int main() { */
/*   listNode* mlist; */
/*   mlist = nodeCreate(22); */
/*   if (!mlist) return -1; */

/*   printlist(mlist); */
/*   return 0; */

/* } */

